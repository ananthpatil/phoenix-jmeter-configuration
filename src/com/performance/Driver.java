package com.performance;


public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {

			String brandName, prptyPath;
			ReadProperties readObj = new ReadProperties();
			if (args.length == 2) {
				brandName = args[0].trim();
				prptyPath = args[1].trim();
				readObj.readProperty(brandName, prptyPath);
			} else {
				brandName = args[0];
				prptyPath = "";
				readObj.readProperty(brandName, prptyPath);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
