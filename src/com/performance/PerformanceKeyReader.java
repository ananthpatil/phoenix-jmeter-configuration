package com.performance;

public class PerformanceKeyReader {
	private String jmeterPath;
	private String funcMode;
	private String consecExe;
	private String testScriptPath;
	private String testDataPath;
	private String rptFolderPath;
	private String confFolderPath;
	private String loops;
	private String numThreads;
	private String rampTime,strtTime,endTime,scheduler,duration,delay;
	private String domain,port,prxyHost,prxyPort,prxyUser,prxyPass,protocol,cntentEncode,path,implType;
	private String strtRps,endRps,durVal,serverPort,serverName;

	public String getJmeterPath() {
		return jmeterPath;
	}

	public void setJmeterPath(String jmeterPath) {
		this.jmeterPath = jmeterPath;
	}

	public String getFuncMode() {
		return funcMode;
	}

	public void setFuncMode(String funcMode) {
		this.funcMode = funcMode;
	}

	public String getConsecExec() {
		return consecExe;
	}

	public void setConsecExec(String consecExe) {
		this.consecExe = consecExe;
	}

	public String getTestScriptPath() {
		return testScriptPath;
	}

	public void setTestScriptPath(String testScriptPath) {
		this.testScriptPath = testScriptPath;
	}

	public String getTestDataPath() {
		return testDataPath;
	}

	public void setTestDataPath(String testDataPath) {
		this.testDataPath = testDataPath;
	}

	public String getResltFldpath() {
		return rptFolderPath;
	}

	public void setResltFldpath(String rptFolderPath) {
		this.rptFolderPath = rptFolderPath;
	}
	
	public String getConfFldpath() {
		return confFolderPath;
	}

	public void setConfFldpath(String confFolderPath) {
		this.confFolderPath = confFolderPath;
	}

	public String getLoopCount() {
		return loops;
	}

	public void setLoopCount(String loops) {
		this.loops = loops;
	}

	public String getNumOfThreads() {
		return numThreads;
	}

	public void setNumOfThreads(String numThreads) {
		this.numThreads = numThreads;
	}

	public String getRampUp() {
		return rampTime;
	}

	public void setRampUp(String rampTime) {
		this.rampTime = rampTime;
	}

	public String getStartTime() {
		return strtTime;
	}

	public void setStartTime(String strtTime) {
		this.strtTime = strtTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getScheduler() {
		return scheduler;
	}

	public void setScheduler(String scheduler) {
		this.scheduler = scheduler;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public String getDelay() {
		return delay;
	}

	public void setDelay(String delay) {
		this.delay = delay;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getPrxyHost() {
		return prxyHost;
	}

	public void setPrxyHost(String prxyHost) {
		this.prxyHost = prxyHost;
	}

	public String getPrxyPort() {
		return prxyPort;
	}

	public void setPrxyPort(String prxyPort) {
		this.prxyPort = prxyPort;
	}

	public String getPrxyUser() {
		return prxyUser;
	}

	public void setPrxyUser(String prxyUser) {
		this.prxyUser = prxyUser;
	}

	public String getPrxyPwd() {
		return prxyPass;
	}

	public void setPrxyPwd(String prxyPass) {
		this.prxyPass = prxyPass;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getContentEncode() {
		return cntentEncode;
	}

	public void setContentEncode(String cntentEncode) {
		this.cntentEncode = cntentEncode;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getImplType() {
		return implType;
	}

	public void setImplType(String implType) {
		this.implType = implType;
	}
	
	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public String getStrtRPS() {
		return strtRps;
	}

	public void setStrtRPS(String strtRps) {
		this.strtRps = strtRps;
	}

	public String getEndRPS() {
		return endRps;
	}

	public void setEndRPS(String endRps) {
		this.endRps = endRps;
	}

	public String getDurationVal() {
		return durVal;
	}

	public void setDurationVal(String durVal) {
		this.durVal = durVal;
	}

}
