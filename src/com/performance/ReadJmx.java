package com.performance;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class ReadJmx {

	
	public static final String CONSTANTS_TESTSCRIPT_PROPERTY_PATH = "\\conf\\testscriptSheeet.properties";
	public static final String CONSTANTS_HTTPDEFAULT_PROPERTY_PATH = "\\conf\\httpdefault.properties";
	public static final String CONSTANTS_THREAD_PROPERTY_PATH = "\\conf\\thread.properties";
	public static final String CONSTANTS_WORKFLOWMODAL_PROPERTY_PATH = "\\conf\\workflow.properties";
	
	Properties propObj;
	//String testScriptPath,testDataPath,rptFolderPath,appUrl,consecExe,funcMode,jmeterPath;
	//String domain,port,prxyHost,prxyPort,prxyUser,prxyPass,protocol,cntentEncode,path,implType,concurrenyPool;
   // String strtRps,endRps,durVal;
	//String forever,loops,numThreads,rampTime,strtTime,endTime,scheduler,duration,delay;
	String cllctPrpID="1586752",strtRpsID="1",endRpsID="48625",durID="48628";
	
	PerformanceKeyReader perfReader = new PerformanceKeyReader();
	/**
	 * This method is used to create the new node and set the value
	 * @param nodeName
	 * @param doc
	 * @param idVal
	 * @param txtVal
	 * @author Anant
	 */
	private void createNodeAndAddValue(Element nodeName,Document doc,String idVal,String txtVal){
	try{
		  Element strtRpNode = doc.createElement("stringProp");
		  strtRpNode.setAttribute("name", idVal);
		  strtRpNode.setTextContent(txtVal.replaceAll("value\\d*", ""));
		  nodeName.appendChild(strtRpNode);
	  }catch(Exception e){
		  System.out.println("The node is not created:"+e);
	  }
	}
	
	/**
	 * The method mis used to create the node with given value
	 * @param chdElements
	 * @param textValue
	 * @author Anant
	 */
	private String createNodeWithValue(NodeList chdElements,int index,String textValue){
		  String idVal = null;
		try{
			Element ele1 = (Element)chdElements.item(index);
			idVal  = ele1.getAttribute("name");
			ele1.setTextContent(textValue.replaceAll("value\\d*", ""));
		}catch(Exception e){
			System.out.println("Exception in creating the node with given value:"+e.getMessage());
		}
		return idVal;
	}
	
	/**
	 * This method is used to add the plus one to string contain
	 * integer value
	 * ]
	 * @param intVal
	 * @return
	 * @author Anant
	 */
	private String addIntVal(String intVal){
		String retVal="";
		if(intVal.matches("\\-*\\d+")){
			Integer inVal = Integer.parseInt(intVal);
			inVal = inVal + 1;
			retVal = inVal.toString();
		}else{
			System.out.println("String doesn't contain integer value:");
			System.exit(0);
		}
		return retVal;
	}

	/**
	 * The method is used to create the the batch file
	 * and Then trigger the execution for JMX file
	 * @param jmeterPath
	 * @param scriptPath
	 * @author Anant
	 */
	private void createAndRunBatcFile(String jmeterPath,String scriptPath){
	 try{
		String driveName = jmeterPath.substring(0,jmeterPath.indexOf(":"));
		File batchFile = new File(System.getProperty("user.dir")+File.separator+"Perf.bat");
		FileOutputStream fOut;
		try {
			fOut = new FileOutputStream(batchFile);
			@SuppressWarnings("resource")
			DataOutputStream dOut = new DataOutputStream(fOut);
			dOut.writeBytes("@echo off\n");
			dOut.writeBytes(driveName+":\n");
			dOut.writeBytes("cd "+jmeterPath+"\n");
			dOut.writeBytes("call jmeter.bat -n -t "+"\""+scriptPath+"\""+"\n");
			
			String cmd="cmd /c start "+System.getProperty("user.dir")+File.separator+"Perf.bat"; 
			Runtime r=Runtime.getRuntime(); 
			@SuppressWarnings("unused")
			Process pr=r.exec(cmd); 
		} catch (FileNotFoundException e) {
			System.out.println("File not found execption while creating the batch file:"+e.getMessage());
		} catch (IOException e) {
			System.out.println("IO Exception while creating the batch file:"+e.getMessage());
		} catch(Exception e){
			System.out.println("The execption in creating the batch file:"+e.getMessage());
		}
	  }catch(Exception e){}
	}
	
	/**
	 * date conversion in Mili seconds
	 * @param date
	 * @return
	 * @author Anant
	 */
	private String dateConversionInMlili(String date) {
		SimpleDateFormat fmt;
		Date d;String dVal = "1479287113000";
		try {
			fmt = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
			d = fmt.parse(date);
			Long dVal1 = d.getTime();
			dVal =  dVal1.toString();
		} catch (ParseException e) {
			System.out.println("date format exception:");
		}
		return dVal;
	}
	
	/**
	 * This method is used to read the test script sheet property file
	 * @return Map
	 * @author Anant
	 */
	public Set<String> readPropertiesData(String prptyPath) {
		propObj = new Properties();
		Set<String> keyDetail = null;
		try {
			String bndName = null;
			keyDetail = new LinkedHashSet<String>();
			propObj.load(new FileInputStream(prptyPath));
			Iterator<Object> it = propObj.keySet().iterator();
			while (it.hasNext()) {
				String val = it.next().toString();
				try {
					bndName = val.substring(0, val.indexOf("."));
					keyDetail.add(bndName.trim());
				} catch (StringIndexOutOfBoundsException a) {
					System.out.println("String Index out of bpound execption "
							+ a.getMessage());
					System.exit(0);
				} catch (NullPointerException e) {
					System.out.println("Null pointer execption in readPropertiesData method :"
							+ e.getMessage());
					System.exit(0);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("The file not found exeception:" + e.getMessage());
		} catch (Exception e) {
			System.out.println("The Exception occurred while reading property: "
					+ prptyPath + "\n" + e.getMessage());
		}
		return keyDetail;
	}


	/**
	 * The method to update the jmx file
	 * @throws TransformerException
	 * @author Anant
	 */
	public void updateJmxFile() throws TransformerException{

		Map<String,String> mpVal =  new LinkedHashMap<String,String>();
		Map<String,String> httpDfmap =  new LinkedHashMap<String,String>();

		// mpVal.put("ThreadGroup.on_sample_error",smpError);
		// mpVal.put("LoopController.continue_forever", forever);
		mpVal.put("LoopController.loops", perfReader.getLoopCount());
		mpVal.put("ThreadGroup.num_threads", perfReader.getNumOfThreads());
		mpVal.put("ThreadGroup.ramp_time", perfReader.getRampUp());
		mpVal.put("ThreadGroup.start_time", perfReader.getStartTime());
		mpVal.put("ThreadGroup.end_time", perfReader.getEndTime());
		mpVal.put("ThreadGroup.scheduler", perfReader.getScheduler());
		mpVal.put("ThreadGroup.duration", perfReader.getDuration());
		mpVal.put("ThreadGroup.delay", perfReader.getDelay());


		httpDfmap.put("HTTPSampler.domain", perfReader.getDomain());
		httpDfmap.put("HTTPSampler.port", perfReader.getPort());
		httpDfmap.put("HTTPSampler.proxyHost", perfReader.getPrxyHost());
		httpDfmap.put("HTTPSampler.proxyPort", perfReader.getPrxyPort());
		httpDfmap.put("HTTPSampler.proxyUser", perfReader.getPrxyUser());
		httpDfmap.put("HTTPSampler.proxyPass", perfReader.getPrxyPwd());
		//  httpDfmap.put("HTTPSampler.connect_timeout", "");
		// httpDfmap.put("HTTPSampler.response_timeout", "");
		httpDfmap.put("HTTPSampler.protocol", perfReader.getProtocol());
		httpDfmap.put("HTTPSampler.contentEncoding", perfReader.getContentEncode());
		httpDfmap.put("HTTPSampler.path", "");
		httpDfmap.put("HTTPSampler.implementation", perfReader.getImplType());
		//httpDfmap.put("HTTPSampler.concurrentPool", "4");



		File jmxFile = new File(perfReader.getTestScriptPath());
		DocumentBuilderFactory dFactory;
		DocumentBuilder dBuilder;
		if(jmxFile.exists()){
			try {
				dFactory = DocumentBuilderFactory.newInstance();
				dBuilder =  dFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(jmxFile);

				doc.getDocumentElement().normalize();
				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

				/**
				 * The following code to update the test plan section
				 */
				try{
					NodeList testPlans =  doc.getElementsByTagName("TestPlan");
					Element tstPlan = (Element)testPlans.item(0);
					NodeList tstPlanChilsEles = tstPlan.getElementsByTagName("boolProp");

					Element ele1 = (Element)tstPlanChilsEles.item(0);
					ele1.setTextContent(perfReader.getFuncMode());

					Element ele2 = (Element)tstPlanChilsEles.item(1); 
					ele2.setTextContent(perfReader.getConsecExec());
				}catch(Exception e){
					System.out.println("The execption occured whie updating the test plan node:"+e.getMessage());
				}


				/**
				 * The following code to set configuration in user
				 * define variables section for result and report
				 * folder path
				 */
				try{
					NodeList userDefinedVar =  doc.getElementsByTagName("Arguments");
					String enabled,attrName,keyTxtVal1="",keyTxtVal2="",keyTxtVal3="";
					int userIncr=0;
					String[] rstPath = perfReader.getResltFldpath().split("#");
					String[] tstPath = perfReader.getTestDataPath().split("#");
					String[] confPath = perfReader.getConfFldpath().split("#");

					for (int u = 0; u < userDefinedVar.getLength(); u++) {
						Element usrElements = (Element)userDefinedVar.item(u);
						enabled = usrElements.getAttribute("enabled");
						if(enabled.toLowerCase().equals("true")){

							try{
								keyTxtVal1 = rstPath[userIncr].trim();
							}catch(Exception e){
								keyTxtVal1 = "";
							}
							try{
								keyTxtVal2 = tstPath[userIncr].trim();
							}catch(Exception e){
								keyTxtVal2 = "";
							}
							try{
								keyTxtVal3 = confPath[userIncr].trim();
							}catch(Exception e){
								keyTxtVal3 = "";
							}
							NodeList userChildEles = usrElements.getElementsByTagName("*");
							for(int u1=0;u1<userChildEles.getLength();u1++){
								Element ele = (Element)userChildEles.item(u1);
								attrName = ele.getAttribute("name");
								if(attrName.trim().toLowerCase().equals("rsltFldPath".toLowerCase())){
									NodeList nlist = ele.getChildNodes();
									nlist.item(3).setTextContent(keyTxtVal1.replaceAll("value\\d*",""));
								}
								if(attrName.trim().toLowerCase().equals("tstDataFldPath".toLowerCase())
										||attrName.trim().equalsIgnoreCase("tstFldpath")){
									NodeList nlist = ele.getChildNodes();
									nlist.item(3).setTextContent(keyTxtVal2.replaceAll("value\\d*",""));
								}
								if(attrName.trim().toLowerCase().equals("confFldPath".toLowerCase())){
									NodeList nlist = ele.getChildNodes();
									nlist.item(3).setTextContent(keyTxtVal3.replaceAll("value\\d*",""));
								}
								if(attrName.trim().toLowerCase().equals("path")){
									NodeList nlist = ele.getChildNodes();
									nlist.item(3).setTextContent(perfReader.getJmeterPath());
								}
							}
							userIncr++;
							//break;
						}
					}
				}catch(Exception e){
					System.out.println("The execption occured while updating the "
							+ "user defined variable"+e.getMessage());
					System.exit(0);
				}



				/**
				 * The following code to update the thread
				 * Groups section.
				 */
				NodeList tdGrpList = doc.getElementsByTagName("ThreadGroup");
				int trdIncr=0;String keyTxtVal;
				try{
					for (int n = 0; n < tdGrpList.getLength(); n++) 
					{
						Element el = (Element) tdGrpList.item(n);
						if (el.getAttribute("enabled").equals("true")) {
							NodeList chdList = el.getElementsByTagName("*");
							Iterator<String> it = mpVal.keySet().iterator();
							boolean flagFound;
							while (it.hasNext()) {
								String keyVal = it.next().trim();
								String[] arrVal = mpVal.get(keyVal).split("#");	
								try{
									keyTxtVal = arrVal[trdIncr].trim();
								}catch(ArrayIndexOutOfBoundsException a){
									keyTxtVal = "";
								}
								flagFound = false;
								for (int c = 2; c < chdList.getLength(); c++) {
									Element tdGrpChdEle = (Element) chdList.item(c);
									String attrVal = tdGrpChdEle.getAttribute("name").trim();
									if (keyVal.trim().equals(attrVal.trim())) {
										flagFound = true;
										if (keyVal.equals("ThreadGroup.start_time") || keyVal.equals("ThreadGroup.end_time")) {
											String strtTim,endTim;
											if (keyVal.contains("start_time")){
												strtTim = dateConversionInMlili(keyTxtVal.replaceAll("value\\d*", "2017/09/19 21:27:40"));
												tdGrpChdEle.setTextContent(strtTim);
											}
											else {
												endTim =  dateConversionInMlili(keyTxtVal.replaceAll("value\\d*", "2017/09/19 21:27:40"));
												tdGrpChdEle.setTextContent(endTim);
											}
										}
										else
										if(attrVal.equals("LoopController.loops")){
											 tdGrpChdEle.setTextContent(keyTxtVal.replaceAll("value\\d*", "-1"));
											/*String val = mpVal.get("LoopController.continue_forever");
											if(val.equals("false")){
											  if (!mpVal.get(attrVal).equals(tdGrpChdEle.getTextContent().trim()))
											      tdGrpChdEle.setTextContent(keyTxtVal.replaceAll("value\\d*", ""));
											}*/
										}
										else
											if (!mpVal.get(attrVal).equals(tdGrpChdEle.getTextContent().trim()))
												tdGrpChdEle.setTextContent(keyTxtVal.replaceAll("value\\d*", ""));
									}
								}
								if (flagFound == false) {
									Element ele = doc.createElement("stringProp");
									ele.setAttribute("name", keyVal);
									ele.setTextContent(keyTxtVal.replaceAll("value\\d*", ""));
									el.appendChild(ele);
								}
							}
							trdIncr++;
						}
					}
				}catch(Exception e){
					System.out.println("Exception occured in while setting the value in thread group:"+e.getMessage());
					System.exit(0);
				}


				/**
				 * The following code to update configuration of 
				 * HTTP Request default section code
				 */
				NodeList httpDeafult = doc.getElementsByTagName("ConfigTestElement");
				int confIncr=0;String keyTxtVal1;
			try{
				for (int h = 0; h < httpDeafult.getLength(); h++) {
					Element mainHttpDefault = (Element) httpDeafult.item(h);
					String confStatus = mainHttpDefault.getAttribute("enabled");
					if (confStatus.equals("true")) {
						NodeList httpDfChdEles = mainHttpDefault.getElementsByTagName("*");
						Iterator<String> it = httpDfmap.keySet().iterator();
						boolean flagFound;
						while (it.hasNext()) {
							String keyVal = it.next();
							String[] arrVal = httpDfmap.get(keyVal).split("#");	
							try{
								keyTxtVal1 = arrVal[confIncr].trim();
							}catch(ArrayIndexOutOfBoundsException a){
								keyTxtVal1 = "";
							}
							flagFound = false;
							for (int nc = 0; nc < httpDfChdEles.getLength(); nc++) {
								Element httpDfEle = (Element) httpDfChdEles.item(nc);
								String attrName = httpDfEle.getAttribute("name");
								if (!attrName.toLowerCase().contains("arguments")) {
									if (keyVal.trim().equals(attrName.trim())) {
										flagFound = true;
										if (!httpDfmap.get(attrName.trim()).equals(httpDfEle.getTextContent().trim()))
											httpDfEle.setTextContent(keyTxtVal1.replaceAll("value\\d*", ""));
									}
								}
							}
							if (flagFound == false) {
								Element ele = doc.createElement("stringProp");
								ele.setAttribute("name", keyVal.trim());
								ele.setTextContent(keyTxtVal1.replaceAll("value\\d*", ""));
								mainHttpDefault.appendChild(ele);
							}
						}
						confIncr++;
						//break;
					}
				}}catch(Exception e){
					System.out.println("Exception occured while setting the value for "
							+ "HTTP Request Default:"+e.getMessage());
					System.exit(0);
				}

				/**
				 * The following code used to set the 
				 * server name and port for HTTP request
				 */
				try{
					NodeList httpSamplerMainNode = doc.getElementsByTagName("HTTPSamplerProxy");
					String tstName,enabled,attrName,srvrPort,srvrName;
					int sampCt=0;
					String[] serverPort = perfReader.getServerPort().split("#");
					String[] serverName = perfReader.getServerName().split("#");
					for(int h=0;h<httpSamplerMainNode.getLength();h++)
					{

						Element httpSamplers = (Element)httpSamplerMainNode.item(h);
						tstName = httpSamplers.getAttribute("testname").toLowerCase();
						enabled = httpSamplers.getAttribute("enabled").toLowerCase();
						if(tstName.contains("serverrequired")&&enabled.contains("true")){
							try{
								srvrName = serverName[sampCt].trim();
							}catch(ArrayIndexOutOfBoundsException a){
								srvrName = "";
							}
							try{
								srvrPort = serverPort[sampCt].trim();
							}catch(ArrayIndexOutOfBoundsException a){
								srvrPort = "";
							}
							NodeList stringProp = httpSamplers.getElementsByTagName("stringProp");
							for(int s=0;s<stringProp.getLength();s++){
								Element eles = (Element)stringProp.item(s);
								attrName = eles.getAttribute("name").trim();
								if(attrName.equals("HTTPSampler.domain")){
									eles.setTextContent(srvrName.replaceAll("value\\d*", ""));
								}
								if(attrName.equals("HTTPSampler.port")){
									eles.setTextContent(srvrPort.replaceAll("value\\d*", ""));
								}
							}
							sampCt++;
							//break;
						}
					}
				}catch(Exception e){
					System.out.println("Exception in adding the server name and port to HTTP Request:"
							+e.getMessage());
				}

				/**
				 * The following code used to set/un-set 
				 * Throughput Timer
				 */
				NodeList throughPutTimer = doc.getElementsByTagName("kg.apc.jmeter.timers.VariableThroughputTimer");
				//int trptIncr=0;String keyTxtVal2;
				for (int h = 0; h < throughPutTimer.getLength(); h++) {
					Element thrghPutTimEle = (Element)throughPutTimer.item(h);
					String confStatus = thrghPutTimEle.getAttribute("enabled");
					if(confStatus.equals("true")){
						NodeList trghTimChdEles = thrghPutTimEle.getElementsByTagName("*");

						NodeList ladElements = thrghPutTimEle.getElementsByTagName("collectionProp");
						Element eles = (Element)ladElements.item(0);

						// if(trghTimChdEles.getLength() > 1){
						if(ladElements.getLength() > 1){

							String[] strtRP = perfReader.getStrtRPS().split("#");
							String[] endRP = perfReader.getEndRPS().split("#");
							String[] durSec = perfReader.getDurationVal().split("#");

							if (strtRP.length == ladElements.getLength() - 1) {
								for (int nc = 1; nc < ladElements.getLength(); nc++) {
									Element trghTimChildEle = (Element)ladElements.item(nc);
									String attrID = trghTimChildEle.getAttribute("name");
									if(attrID.matches("\\-*\\d+")){
										NodeList chdElements =  trghTimChildEle.getElementsByTagName("*");
										strtRpsID = createNodeWithValue(chdElements,0,strtRP[nc-1]);
										endRpsID = createNodeWithValue(chdElements,1,endRP[nc-1]);
										durID = createNodeWithValue(chdElements,2,durSec[nc-1]);
									}
								}
							}
							if(strtRP.length < ladElements.getLength() - 1){
								for (int nc = ladElements.getLength()-1; nc >= 1; nc--) {
									if (nc != strtRP.length) {
										//try{
										Element trghTimChildEle = (Element)ladElements.item(nc);
										eles.removeChild(trghTimChildEle);
										//}catch(Exception e){}
									}
									if(nc==strtRP.length || nc<strtRP.length){	
										for (int s = 0; s < strtRP.length; s++) {
											Element trghTimChildEle = (Element)ladElements.item(nc);
											String attrID = trghTimChildEle.getAttribute("name");

											if(attrID.matches("\\-*\\d+")){

												NodeList chdElements =  trghTimChildEle.getElementsByTagName("*");
												strtRpsID = createNodeWithValue(chdElements,0,strtRP[s]);
												endRpsID = createNodeWithValue(chdElements,1,endRP[s]);
												durID = createNodeWithValue(chdElements,2,durSec[s]);
											}
										}
									}
								}  
							}
							if(strtRP.length > ladElements.getLength() - 1){
								int nc;String attrID = "";
								for (nc = 1; nc < ladElements.getLength(); nc++) {
									Element trghTimChildEle = (Element)ladElements.item(nc);
									attrID = trghTimChildEle.getAttribute("name");
									if(attrID.matches("\\-*\\d+")){
										NodeList chdElements =  trghTimChildEle.getElementsByTagName("*");
										strtRpsID = createNodeWithValue(chdElements,0,strtRP[nc-1]);
										endRpsID = createNodeWithValue(chdElements,1,endRP[nc-1]);
										durID = createNodeWithValue(chdElements,2,durSec[nc-1]);
									}
								}for (int s = nc-1; s <strtRP.length; s++) {

									cllctPrpID = attrID;

									cllctPrpID = addIntVal(cllctPrpID);
									strtRpsID = addIntVal(strtRpsID);
									endRpsID = addIntVal(endRpsID);
									durID = addIntVal(durID);

									Element clctProp = doc.createElement("collectionProp");
									clctProp.setAttribute("name", cllctPrpID);
									eles.appendChild(clctProp);

									createNodeAndAddValue(clctProp, doc, strtRpsID, strtRP[s]);
									createNodeAndAddValue(clctProp, doc, endRpsID, endRP[s]);
									createNodeAndAddValue(clctProp, doc, durID, durSec[s]);
								}
							}
						}
						if (ladElements.getLength() == 1) {

							String[] strtRP = perfReader.getStrtRPS().split("#");
							String[] endRP = perfReader.getEndRPS().split("#");
							String[] durSec = perfReader.getDurationVal().split("#");
							if ((strtRP.length == endRP.length && durSec.length == strtRP.length)
									&& endRP.length == durSec.length) {
								Node mainNode = trghTimChdEles.item(0);
								System.out.println("main node:"+mainNode);

								for (int s = 0; s < strtRP.length; s++) {
									if (s == 0) {
										Element clctProp = doc.createElement("collectionProp");
										clctProp.setAttribute("name", cllctPrpID);
										mainNode.appendChild(clctProp);

										createNodeAndAddValue(clctProp, doc, strtRpsID, strtRP[s]);
										createNodeAndAddValue(clctProp, doc, endRpsID, endRP[s]);
										createNodeAndAddValue(clctProp, doc, durID, durSec[s]);
									}else{
										cllctPrpID = addIntVal(cllctPrpID);
										strtRpsID =  addIntVal(strtRpsID);
										endRpsID =   addIntVal(endRpsID);
										durID =      addIntVal(durID);

										Element clctProp = doc.createElement("collectionProp");
										clctProp.setAttribute("name", cllctPrpID);
										mainNode.appendChild(clctProp);

										createNodeAndAddValue(clctProp, doc, strtRpsID, strtRP[s]);
										createNodeAndAddValue(clctProp, doc, endRpsID, endRP[s]);
										createNodeAndAddValue(clctProp, doc, durID, durSec[s]);
									}
								}  
							}else{
								System.out.println("Pass the same no of value for strtRps,endRPs and durVal:");
								System.exit(0);
							}
						}
					}
				}
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(perfReader.getTestScriptPath());
				transformer.transform(source, result);
				System.out.println("====The succesfully updated the jmx file===");
				createAndRunBatcFile(perfReader.getJmeterPath(),perfReader.getTestScriptPath());

			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			System.out.println("The file not exist in given path:"+perfReader.getTestScriptPath());
		}
	}
	}