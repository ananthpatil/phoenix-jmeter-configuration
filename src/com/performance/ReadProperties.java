package com.performance;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

import javax.xml.transform.TransformerException;

public class ReadProperties extends ReadJmx{
	
	public Set<String> prptyData;
	String brandName = null;
	boolean brandFound = false;
	
	/**
	 * The following method used to read the properties files
	 * @param bndName
	 * @param prptyPath
	 * @throws TransformerException
	 * @author Anant
	 */
	
	public void readProperty(String bndName,String prptyPath) throws TransformerException{
		
		       Properties prop = new Properties();
		       String tstScrptPrptyPath, threadPrptyPath, httpDefaultPrptyPath, wrkFlowPrptyPath;
		
			/**
			 * The following code used to get all properties keys From test script
			 * sheet properties file set the boolean value true if given brand name
			 * found in property file.
			 */
			if (prptyPath.length() == 0) {
			     tstScrptPrptyPath = System.getProperty("user.dir") + CONSTANTS_TESTSCRIPT_PROPERTY_PATH.replace("\\", File.separator).replace("/", File.separator);
			     threadPrptyPath = System.getProperty("user.dir")   + CONSTANTS_THREAD_PROPERTY_PATH.replace("\\", File.separator).replace("/", File.separator);
			     httpDefaultPrptyPath = System.getProperty("user.dir") + CONSTANTS_HTTPDEFAULT_PROPERTY_PATH.replace("\\", File.separator).replace("/", File.separator);
			     wrkFlowPrptyPath = System.getProperty("user.dir") + CONSTANTS_WORKFLOWMODAL_PROPERTY_PATH.replace("\\", File.separator).replace("/", File.separator);
			} else {
				 System.out.println("sontrol eneter here");
				 tstScrptPrptyPath = prptyPath +  File.separator + "testscriptSheeet.properties";
				 threadPrptyPath = prptyPath + File.separator + "thread.properties";
				 httpDefaultPrptyPath = prptyPath + File.separator + "httpdefault.properties";
				 wrkFlowPrptyPath = prptyPath + File.separator + "workflow.properties";		
			}
			prptyData = readPropertiesData(tstScrptPrptyPath);
			Iterator<String> repeat = prptyData.iterator();
			while (repeat.hasNext()) {
				if (repeat != null) {
					brandName = repeat.next();
					if (bndName.trim().equals(brandName.trim())) {
						System.out.println("The given brandname found in property file hence execution will start "
								+ brandName.trim());
						brandFound = true;
						break;
					}
				}
			}
			if (brandFound) {
				/**
				 * The following code for reading the test script sheet details From SLF.
				 * driver properties file.
				 */
					try {
				perfReader.setJmeterPath(propObj.getProperty("jmeter.path")
						.trim().replace("\\", File.separator)
						.replace("/", File.separator));
				perfReader.setFuncMode(propObj
						.getProperty("testPlan.functionalMode").trim()
						.replace("\\", File.separator)
						.replace("/", File.separator));
				perfReader.setConsecExec(propObj
						.getProperty("testPlan.consecutiveExe").trim()
						.replace("\\", File.separator)
						.replace("/", File.separator));
				// jmeterPath =
				// propObj.getProperty("jmeter.path").trim().replace("\\",
				// File.separator).replace("/", File.separator);
				// funcMode =
				// propObj.getProperty("testPlan.functionalMode").trim().replace("\\",
				// File.separator).replace("/", File.separator);
				// consecExe =
				// propObj.getProperty("testPlan.consecutiveExe").trim().replace("\\",
				// File.separator).replace("/", File.separator);
				perfReader.setTestScriptPath(propObj
						.getProperty(brandName + ".scriptPath").trim()
						.replace("\\", File.separator)
						.replace("/", File.separator));
				perfReader.setTestDataPath(propObj
						.getProperty(brandName + ".tstDataFldPath").trim()
						.replace("\\", File.separator)
						.replace("/", File.separator));
				perfReader.setResltFldpath(propObj
						.getProperty(brandName + ".rsltFldPath").trim()
						.replace("\\", File.separator)
						.replace("/", File.separator));
				perfReader.setConfFldpath(propObj
						.getProperty(brandName + ".confFldPath").trim()
						.replace("\\", File.separator)
						.replace("/", File.separator));
						
						//testScriptPath = propObj.getProperty(brandName + ".scriptPath").trim().replace("\\", File.separator).replace("/", File.separator);
						//testDataPath = propObj.getProperty(brandName + ".tstDataFldPath").trim().replace("\\", File.separator).replace("/", File.separator);
						//rptFolderPath = propObj.getProperty(brandName + ".rsltFldPath").trim().replace("\\", File.separator).replace("/", File.separator);
						//appUrl = propObj.getProperty(brandName + ".appUrl").trim();
					} catch (NullPointerException n) {
						System.out.println("Null pointer exception while reading test script sheet property file:"
								+ n.getMessage());
						System.exit(0);
					} catch (Exception e) {
				  	    System.out.println("Exception while reading test script sheet properties file:"
							+ e.getMessage());
				  	  System.exit(0);
				   }

				/**
				 * The following code used to read configuration details
				 * of thread group from
				 * threadPrptyPath property file
				 */
				try{
					  prop.load(new FileInputStream(threadPrptyPath));
				try {
					//forever = prop.getProperty(brandName + ".continue_forever").trim();
					perfReader.setLoopCount(prop.getProperty(brandName + ".loops").trim());
					perfReader.setNumOfThreads(prop.getProperty(brandName + ".numThreads").trim());
					perfReader.setRampUp(prop.getProperty(brandName + ".rampTime").trim());
					perfReader.setStartTime(prop.getProperty(brandName + ".startTime").trim());
					perfReader.setEndTime(prop.getProperty(brandName + ".endTime").trim());
					perfReader.setScheduler(prop.getProperty(brandName + ".scheduler").trim());
					perfReader.setDuration(prop.getProperty(brandName + ".duration").trim());
					perfReader.setDelay(prop.getProperty(brandName + ".delay").trim());
					
				/*	loops = prop.getProperty(brandName + ".loops").trim();
					numThreads =  prop.getProperty(brandName + ".numThreads").trim();
					rampTime = prop.getProperty(brandName + ".rampTime").trim();
					strtTime = prop.getProperty(brandName + ".startTime").trim();
					endTime = prop.getProperty(brandName + ".endTime").trim();
					scheduler = prop.getProperty(brandName + ".scheduler").trim();
					duration = prop.getProperty(brandName + ".duration").trim();
					delay = prop.getProperty(brandName + ".delay").trim();*/
					
				} catch (NullPointerException n) {
					System.out.println("Null pointer exeception occured while reading the thread properties file:"
							+ n.getMessage());
					System.exit(0);
				  }
			    }catch(FileNotFoundException e){
			    	System.out.println("File not found "+threadPrptyPath+" Exception"+e.getMessage());
			    	System.exit(0);
			    } catch (Exception e) {
					System.out.println("Execption while reading the thread propert file");
				}
				
				/**
				 * The following code used to read the HTTP default
				 * configuration element details from the
				 * httpDefaultPrptyPath property file 
				 */
				try{
					  prop.load(new FileInputStream(httpDefaultPrptyPath));
				try {
					perfReader.setDomain(prop.getProperty(brandName + ".domain").trim());
					perfReader.setPort(prop.getProperty(brandName + ".port").trim());
					perfReader.setPrxyHost(prop.getProperty(brandName + ".prxyHost").trim());
					perfReader.setPrxyPort(prop.getProperty(brandName + ".prxyPort").trim());
					perfReader.setPrxyUser(prop.getProperty(brandName + ".prxyUser").trim());
					perfReader.setPrxyPwd(prop.getProperty(brandName + ".prxyPass").trim());
					perfReader.setProtocol(prop.getProperty(brandName + ".protocol").trim());
					perfReader.setContentEncode(prop.getProperty(brandName + ".contentEncoding").trim());
					perfReader.setPath(prop.getProperty(brandName + ".path").trim());
					perfReader.setImplType(prop.getProperty(brandName + ".implementation").trim());
					perfReader.setServerName(prop.getProperty(brandName + ".severName").trim());
					perfReader.setServerPort(prop.getProperty(brandName + ".severPort").trim());
					
					/*domain = prop.getProperty(brandName + ".domain").trim();
					port =  prop.getProperty(brandName + ".port").trim();
					prxyHost = prop.getProperty(brandName + ".prxyHost").trim();
					prxyPort = prop.getProperty(brandName + ".prxyPort").trim();
					prxyUser = prop.getProperty(brandName + ".prxyUser").trim();
					prxyPass = prop.getProperty(brandName + ".prxyPass").trim();
					protocol = prop.getProperty(brandName + ".protocol").trim();
					cntentEncode = prop.getProperty(brandName + ".contentEncoding").trim();
					path = prop.getProperty(brandName + ".path").trim();
					implType = prop.getProperty(brandName + ".implementation").trim();*/
					
				} catch (NullPointerException n) {
					System.out.println("Null pointer exeception occured while reading the test script properties file:"
							+ n.getMessage());
					System.exit(0);
				  }
			    }catch(FileNotFoundException e){
			    	System.out.println("File not found "+threadPrptyPath+" Exception"+e.getMessage());
			    	System.exit(0);
			    } catch (Exception e) {
					System.out.println("Execption while reading the thread property file");
				}
				
				/**
				 * The following code is used to read the configuration details 
				 * of through put shape timer
				 * from wrkFlowPrptyPath files
				 */
				try{
					  prop.load(new FileInputStream(wrkFlowPrptyPath));
				try {
					perfReader.setStrtRPS(prop.getProperty(brandName + ".startRps").trim());
					perfReader.setEndRPS(prop.getProperty(brandName + ".endRps").trim());
					perfReader.setDurationVal(prop.getProperty(brandName + ".durationVal").trim());
					
					/*strtRps = prop.getProperty(brandName + ".startRps").trim();
					endRps =  prop.getProperty(brandName + ".endRps").trim();
					durVal = prop.getProperty(brandName + ".durationVal").trim();*/
				} catch (NullPointerException n) {
					System.out.println("Null pointer exeception occured while reading the test script properties file:"
							+ n.getMessage());
					System.exit(0);
				  }
			    }catch(FileNotFoundException e){
			    	System.out.println("File not found "+wrkFlowPrptyPath+" Exception"+e.getMessage());
			    	System.exit(0);
			    } catch (Exception e) {
					System.out.println("Execption while reading the wrkFlowPrptyPath property file");
				}	
				
				updateJmxFile();
				
			} else {
				System.out.println("The given brand couldn't found in property file please look the property file");
			}
	   }
}
